/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
*/

import java.util.HashMap;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Time;


public class Pratica61 {
    public static void main(String[] args) {
    Time time1 = new Time();
    Time time2 = new Time();
    
    time1.addJogador("Goleiro", new Jogador(1, "Fulano"));
    time1.addJogador("Lateral", new Jogador(4, "Ciclano"));
    time1.addJogador("Atacante", new Jogador(10, "Beltrano"));
    
    time2.addJogador("Atacante", new Jogador(15, "Mario"));
    time2.addJogador("Lateral", new Jogador(7, "José"));
    time2.addJogador("Goleiro", new Jogador(1, "João"));
   
    System.out.println("Posição\t\t\t\tTime 1\t\t\t\tTime 2");
        
    for(int i = 0; i<time1.getJogadores().size(); i++)
        System.out.println(time1.getJogadores().keySet().toArray()[i] + "\t\t\t\t" + time1.getJogadores().get(time1.getJogadores().keySet().toArray()[i]) + "\t\t\t" + time2.getJogadores().get(time2.getJogadores().keySet().toArray()[i]));
  
    
    }
}